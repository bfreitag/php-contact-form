<?php
class Mail {
    public static function createBody($inputArray): string
    {

        return '<h2>Contact Request Submitted</h2>
                <div>
                    <h4>Name</h4>' . $inputArray['fullName'] . '
                </div>
                <div>
                    <h4>Email</h4>' . $inputArray['email'] . '
                </div>
                <div>
                    <h4>Phone</h4>' . (isset($inputArray['phone']) ? $inputArray['phone'] : "") . '
                </div>
                <div>
                    <h4>Message</h4>' . $inputArray['message'] . '
                </div>';
    }

    public static function sendEmail($subject, $message, $to = 'guy-smiley@example.com'): bool
    {
        // Returns true if sent, false otherwise
        return mail($to, $subject, $message);
    }
}