<?php
class Validate {
    public static function validation($inputArray) {
        $msg = '';
        if ($msg = self::_name($inputArray['fullName'])) {
            return $msg;
        }
        if ($msg = self::_email($inputArray['email'])) {
            return $msg;
        }
        if(!empty($inputArray['phone'])) {
            if ($msg = self::_phone($inputArray['phone'])) {
                return $msg;
            }
        }
        if ($msg = self::_message($inputArray['message'])) {
            return $msg;
        }
        return $msg;
    }

    private static function _name($name) {
        if (strlen($name) < 3) {
            return 'Name is too short.';
        } elseif (strlen($name) > 255) {
            return 'Name is too long.';
        }

        return false;
    }

    private static function _email($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return 'Email is invalid.';
        }
        return false;
    }

    private static function _phone($phone) {
        $phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        $phone = str_replace("-", "", $phone);
        if (strlen($phone) < 10 || strlen($phone) > 14) {
            return 'Phone Number is not valid.';
        }
        return false;
    }

    private static function _message($message) {
        if (strlen($message) < 1) {
            return 'Please add a message.';
        }

        return false;
    }
}