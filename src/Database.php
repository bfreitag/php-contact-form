<?php
class Database {
    private static function connect($db = '') {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $mysqli = new mysqli("127.0.0.1", "root", "password", $db);
        if ($mysqli->connect_error) {
            die();
        }
        return $mysqli;
    }

    public static function create() {
        $mysqli = self::connect();

        // Create database
        $sql = "CREATE DATABASE IF NOT EXISTS contactform";
        if ($mysqli->query($sql) !== TRUE) {
            $mysqli->close();
            return false;
        }
        $mysqli->close();
        $mysqli = self::connect('contactform');
        // sql to create table
        $sql = "CREATE TABLE IF NOT EXISTS Contacts (
            id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            fullname VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            phone VARCHAR(255),
            message VARCHAR(1024) NOT NULL,
            date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            )";

        $return = $mysqli->query($sql);

        $mysqli->close();
        return $return;
    }

    public static function storeContacts($inputArray): bool
    {
        $mysqli = self::connect('contactform');
        $contact = implode('","', $inputArray);
        $sql = 'INSERT INTO Contacts (fullname, email, message, phone)
            VALUES ("' . $contact . '")';

        $statement = $mysqli->prepare($sql);
        $return = $statement->execute();
        $mysqli->close();
        return $return;
    }
}