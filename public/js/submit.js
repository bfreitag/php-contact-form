
$('#contact-form').submit(function (e) {
    e.preventDefault();
    console.log(e)
    $.ajax({
        url: "contact.php",
        data: {
            'fullName': $("#fullName").val(),
            'email': $("#email").val(),
            'phone': $("#phone").val(),
            'message': $("#message").val()
        },
        method: "POST",
        success: function (response) {
            let data = JSON.parse(response);
            $(".statusMsg").removeClass('text-danger text-success');

            if (data.msg) {
                $(".statusMsg").addClass('text-danger');
            } else {
                $(".statusMsg").addClass('text-success');
            }
            $(".statusMsg").show('fast');
            $(".statusMsg").text(data.msg ? data.msg : 'Your request has been processed!');
            $(".statusMsg").fadeOut(7500);
            $("#contact-form").each(function () {
                this.reset();
            });
        }
    });
});