<?php
include "../src/autoload.php";

// Check that POST data and POST submit exists
if ($_POST) {
    $fullName = htmlentities(strip_tags($_POST['fullName']));
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = htmlentities(strip_tags($_POST['message']));

    // Make sure values are not empty
    if(!empty($fullName) && !empty($email) && !empty($message)) {
        $inputArray = [
            'fullName' => $fullName,
            'email' => $email,
            'message' => $message,
            'phone' => $phone
        ];

        $validate = new Validate();
        $validationErrors = $validate::validation($inputArray);
        if ($validationErrors) {
            echo json_encode(array("success" => 0, 'msg' => $validationErrors));
            exit;
        }

        $mail = new Mail();
        $html = $mail::createBody($inputArray);
        $subject = 'Contact Form Submission';
        $sent = $mail::sendEmail($subject, $html);

//        if ($sent === false) {
//            echo json_encode(array("success" => 0, 'msg' => 'Error sending contact form request. Please try again.'));
//            exit;
//        }

        $db = new Database();
        $create = $db::create();
        if ($create === false) {
            echo json_encode(array("success" => 0, 'msg' => 'Error sending contact form request. Please try again.'));
            exit;
        }
        $store = $db::storeContacts($inputArray);
        if ($store === false) {
            echo json_encode(array("success" => 0, 'msg' => 'Error sending contact form request. Please try again.'));
            exit;
        }
        echo json_encode(array("success" => 1, 'msg' => ""));
    }
}