<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
include "../src/autoload.php";
final class DatabaseTest extends TestCase
{
    public function testConnectionOk() {
        $database = Database::create();
        $this->assertEquals(true, $database);
    }
}
