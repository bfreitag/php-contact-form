<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
include "../src/autoload.php";

final class ValidateTest extends TestCase
{
    public function testValidationOk()
    {
        $input = array('fullName' => 'Joe Smith', 'email' => 'test@gmail.com', 'message' => 'Testing');
        $errorsArray = Validate::validation($input);
        $this->assertEquals(false, $errorsArray);
    }

    public function testValidationFailWithoutName()
    {
        $input = array('fullName' => '', 'email' => 'john@gmail.com', 'message' => 'Testing');
        $errorsArray = Validate::validation($input);
        $this->assertEquals('Name is too short.', $errorsArray);
    }

    public function testValidationFailWithInvalidEmail()
    {
        $input = array('fullName' => 'John', 'email' => 'john.com', 'message' => 'Testing');
        $errorsArray = Validate::validation($input);
        $this->assertEquals('Email is invalid.', $errorsArray);
    }

    public function testValidationFailWithInvalidPhone()
    {
        $input = array('fullName' => 'John', 'email' => 'john@test.com', 'message' => 'Testing', 'phone' => '123456a');
        $errorsArray = Validate::validation($input);
        $this->assertEquals('Phone Number is not valid.', $errorsArray);
    }

    public function testValidationPassWithPhone()
    {
        $input = array('fullName' => 'John', 'email' => 'john@test.com', 'message' => 'Testing', 'phone' => '555-648-1322');
        $errorsArray = Validate::validation($input);
        $this->assertEquals(false, $errorsArray);
    }
}
