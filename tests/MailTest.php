<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once "../src/Mail.php";

final class MailTest extends TestCase
{
    public function testValidationCreateMessage() {
        $input = array('fullName' => 'Joe Smith', 'email' => 'test@gmail.com', 'message' => 'This is my message');
        $emailBody = Mail::createBody($input);
        $this->assertEquals(
            '<h2>Contact Request Submitted</h2>
                <div>
                    <h4>Name</h4>Joe Smith
                </div>
                <div>
                    <h4>Email</h4>test@gmail.com
                </div>
                <div>
                    <h4>Phone</h4>
                </div>
                <div>
                    <h4>Message</h4>This is my message
                </div>', $emailBody);
    }
}
